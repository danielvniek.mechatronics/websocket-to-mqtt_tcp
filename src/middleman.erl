
-module(middleman).
-author("Daniel van Niekerk").
-behaviour(gen_server).


-export([start_link/0]).
-export([init/1, handle_call/3, handle_cast/2, handle_info/2, terminate/2,
  code_change/3]).

-define(SERVER, ?MODULE).

-record(mm_state, {ws_pid,connection_pid,topics,host,port,username,password}).%%note that for now the default mod is always used but in future developments this can be changed to be more like the core components' receptions where the storage method can be changed

%%%===================================================================
%%% Spawning and gen_server implementation
%%%===================================================================

start_link() ->
  gen_server:start_link({local, mm}, ?MODULE, [], []).

init([]) ->
  yes = global:register_name(mm,self()),
  
  {ok, #mm_state{connection_pid=none}}.%%unready_ids used for factory startups

handle_call({new_ws,WSPid},_From,State)->
	{reply,ok,State#mm_state{ws_pid=WSPid}};

handle_call({<<"connect">>,ContentS},_From,State)->
	io:format("\nContentS:~p",[ContentS]),
	Content = jsone:decode(ContentS,[{object_format, map}]),
	case State#mm_state.connection_pid of
		none->
			ok;
		_->
			case is_process_alive(State#mm_state.connection_pid) of
				true->
					unlink(State#mm_state.connection_pid),
					exit(State#mm_state.connection_pid,kill);
				_->
					ok
			end
	end,		
	UserN = binary_to_list(maps:get(<<"username">>,Content)),
	Passw = binary_to_list(maps:get(<<"password">>,Content)),
	Port = maps:get(<<"port">>,Content),
	Host = binary_to_list(maps:get(<<"host">>,Content)),
	io:format("\nStarting emqtt"),
	{ok, ConnPid} = emqtt:start_link([{host,Host},{username,UserN},{password,Passw}]),
	try_to_connect(ConnPid,[]),
	{reply,ok,State#mm_state{connection_pid = ConnPid,host=Host,username=UserN,password=Passw,topics=[]}};			

handle_call({<<"subscribe">>,Content},_From,State)->
	Topic = Content,
	SubOpts = [{qos, 1}],
	case is_pid(State#mm_state.connection_pid) and is_process_alive(State#mm_state.connection_pid) of
		true->
			{SubRes, _Props, _ReasonCodes} = emqtt:subscribe(State#mm_state.connection_pid, #{}, [{Topic, SubOpts}]),
			case SubRes of
				ok->
					io:format("\nSuccessfully subsribed to ~p",[Topic]);
				_->
					io:format("\nCould not subsribe to ~p",[Topic])
			end;
		_->
			io:format("\nCould not subscribe because not connected")
	end,
	NewTopics = [Topic|State#mm_state.topics],
	{reply,ok,State#mm_state{topics = NewTopics}};

handle_call({<<"publish">>,ContentS},_From,State)->
	io:format("\nContentS:~p",[ContentS]),
	Content = jsone:decode(ContentS,[{object_format, map}]),
	case State#mm_state.connection_pid of
		none->
			io:format("\nCannot publish because not connected"),
			ok;
		_->
			ok = emqtt:publish(State#mm_state.connection_pid, maps:get(<<"topic">>,Content), #{}, maps:get(<<"payload">>,Content), [{qos, 0}]),
			io:format("\nPublished ~p for topic:~p",[maps:get(<<"payload">>,Content),maps:get(<<"topic">>,Content)])
	end,		
	{reply,ok,State};			


handle_call(Unknown, _From, State) ->
	io:format("\nMM got unknown request:~p",[Unknown]),
  {reply, {bad_request,Unknown}, State}.

handle_cast(_Request, State) ->
  {noreply, State}.

handle_info(Info, State) ->
	io:format("\nInfo:~p",[Info]),
	case Info of
		{disconnect, ReasonCode, Properties} ->
	        io:format("\nNETW ERROR: Recv a DISCONNECT packet from ~p for ReasonCode: ~p, Properties: ~p~n", [State#mm_state.host,ReasonCode, Properties]),
			try_to_connect(State#mm_state.connection_pid,State#mm_state.topics);
	    {publish, PUBLISH} ->
			Topic = maps:get(topic,PUBLISH),
			Payload = maps:get(payload,PUBLISH),
			Pub = #{<<"topic">>=>Topic,<<"payload">>=>Payload},
			PUB = jsone:encode(Pub),
			WSPID = State#mm_state.ws_pid,
	        WSPID!{send,PUB}
	end,
  {noreply, State}.

terminate(_Reason, _State) ->
  ok.

code_change(_OldVsn, State, _Extra) ->
  {ok, State}.


try_to_connect(ConnPid,Topics)->
	io:format("\nTrying to connect to broker"),
	Res = emqtt:connect(ConnPid),
	case Res of
		{ok,_Props}->
			io:format("\nConnected"),
			ok,
			subscribe_to_topics(Topics,ConnPid);
		_->
			io:format("\nRes when trying to connect to broker: ~p",[Res]),
			error
	end.

subscribe_to_topics([],_ConnPid)->
	ok;
subscribe_to_topics([Topic|T],ConnPid)->
	SubOpts = [{qos, 1}],
	{ok, _Props, _ReasonCodes} = emqtt:subscribe(ConnPid, #{}, [{Topic, SubOpts}]),
	subscribe_to_topics(T,ConnPid).




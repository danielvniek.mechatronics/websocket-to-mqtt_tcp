-module(dash_socket).

-export([init/2]).
-export([websocket_init/1]).
-export([websocket_handle/2]).
-export([websocket_info/2]).


init(Req, Opts) ->
  {cowboy_websocket, Req, Opts,#{idle_timeout=>360000000}}.%6000 min timeout

websocket_init(State) ->
	%error_log:log(?MODULE,0,unknown,"\nNew websocket ~p",[self()]),
	MyPid = self(),
	gen_server:call(mm,{new_ws,MyPid}),
  {[], State}.

websocket_handle({text, Msg_in}, State) ->
	%error_log:log(?MODULE,0,unknown,"Msg_in:~p",[Msg_in]),
  SearchMap = jsone:decode(Msg_in,[{object_format, map}]),
 % error_log:log(?MODULE,0,unknown,"SearchMap:~p",[SearchMap]),
  Request = maps:get(<<"Request">>,SearchMap),
  Content = maps:get(<<"Content">>,SearchMap),
  gen_server:call(mm,{Request,Content}),
  {ok, State};

websocket_handle(_Data, State) ->
	%error_log:log(?MODULE,0,unknown,"\nws handle ~p",[self()]),
  {[], State}.
	
websocket_info({timeout, _Ref, Msg}, State) ->
	%error_log:log(?MODULE,0,unknown,"\nws timeout info ~p",[self()]),
  erlang:start_timer(1000, self(), <<"How' you doin'?">>),
  {[{text, Msg}], State};

websocket_info({send,Msg},State)->
	io:format("\nMsg received by dash socket:~p",[Msg]),
	%error_log:log(?MODULE,0,unknown,"\nMsg:~p",[Msg]),
  {[{text,Msg}], State};
websocket_info(_Info, State) ->
	io:format("\nUnrecognized info"),
	%error_log:log(?MODULE,0,unknown,"\nws info ~p for pid ~p",[Info,self()]),
  {[], State}.


%%SLOC:61



	
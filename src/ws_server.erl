%% Feel free to use, reuse and abuse the code in this file.

%% @private
-module(ws_server).


%% API.
-export([start/2]).
-export([stop/1]).

%% API.
start(_Type, _Args) ->
  Dispatch = cowboy_router:compile([
      {'_', [
           % {"/", cowboy_static, {file,"./web_dash/index.html"}},
            {"/websocket", dash_socket, []}
            ]}
  ]),
  {ok, _} = cowboy:start_clear(web_dash, [{port, 8000}], #{
    env => #{dispatch => Dispatch}
  }).

stop(_State) ->
  ok = cowboy:stop_listener(web_dash).

%%SLOC:15
%% @author Daniel
%% @doc @todo Add description to bf.


-module(bf).

%% ====================================================================
%% API functions
%% ====================================================================
-export([start/0]).



%% ====================================================================
%% Internal functions
%% ====================================================================
start()->
	true = code:add_pathz("./cowboy_and_ranch"),
	true = code:add_pathz("./emqtt"),
	ok = application:start(ssl),
	ok = application:start(ranch),
	ok = application:start(cowlib),
	ok = application:start(cowboy),
	ok = application:start(ws_to_mqtt).
	


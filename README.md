# Description #
This project is a tool that allows a web application to communicate with an MQTT broker that does not have a websocket port, but only a TCP port. This project was initially developed to accomodate a Vuforia application (https://developer.vuforia.com/) to communicate with the LernFabrik 4.0 (https://www.fischertechnik.de/en/service/elearning/teaching/lernfabrik-4), for the purposes of controlling the LernFabrik 4.0 with the Microsoft Hololens (https://www.microsoft.com/en-us/hololens).

# Prerequisites #
Erlang 22.2 or higher

# How to use #
Follow this short video\
https://youtu.be/qDkbqNrse2o

# JSON Messages between Web App and Erlang #
Please first watch the above video and use the code snippets below only as quick references
### Connect to Erlang application from web app ###
```
var wsHost = "ws://localhost:8000/websocket";
var websocket = new WebSocket(wsHost);
websocket.onmessage = function(evt){onMessage(evt)};
```
### Connect to broker ###
```
var ConMsg = {"Request":"connect","Content":"{\"host\":\"broker.hivemq.com\",\"port\":1883,\"username\":\"asdf\",\"password\":\"qwer\"}"};
\\Replace host, port, username and password as required
websocket.send(ConMsg);
```
### Subscribe to MQTT topic ###
```
var SubMsg = {"Request":"subscribe","Content":"topic"};
//Replace topic as required
websocket.send(SubMsg);
```

### Publish to MQTT topic ###
```
var PubMsg = {"Request":"publish","Content":"{\"topic\":\"topic\",\"payload\":\"hello\"}"};
//Replace topic and payload as required
websocket.send(PubMsg);
```
### Receive published message on a subscribed topic ###
```
function onMessage(evt){
    var Msg = JSON.parse(message.data);
    var MsgMap = JSON.parse(Msg);
    var Topic = MsgMap.Topic;
    var Payload = MsgMap.Payload;
}
```

# Testing #
Use https://www.hivemq.com/public-mqtt-broker/ as MQTT broker and https://www.piesocket.com/websocket-tester as web application as shown in video

